---
title: Changelog
---

# Changelog

## V1.5.0
- Visualizar os recursos de uma aplicação no formato de árvore;
- Mostrar arquivos alterados no editor de código;

## V1.4.0
- Conectar repos e providers através de OAuth2;
- Exibir listagem de aplicações no formato de cards;

## V1.4.0
- Criado uma tela para download do CLI;
- Criação de aplicações a partir de YAML;
- Visualizar aplicações como componentes na listagem;
- Configurar Self Heal da aplicação pelo Planktron;
- Para Azure e GCP foi removido a escolha do tipo de rede na criação do cluster;
- Corrigido erro no frontend na criação de repo privado sem permissão;
- Corrigido erro ao abrir aba de Sync após excluir algum recurso da aplicação;
- Corrigido o scroll da listagem de aplicações;
- Melhorado a usabilidade da tela de credenciais;
- Melhorias nas compositions de cluster;
- Agora o CLI possui um Wizard de conexão de cluster através do Kubeconfig;
- Na tela de conectar cluster agora tem um passo a passo para instalar o CLI e conectar um cluster via linha de comando;
- Não é mais permitido excluir o projeto Default;
- Criado um Quick Start com cards de ajuda para novas organizações;
- Ajustes diversos, correções de bugs e melhorias;

## V1.3.1
- Permitir utilizar o CLI usando os nomes dos recursos;
- Novo credencial cloud: Azure;
- Correção de bugs e pequenos ajustes;
- Implementado Ignore Differences na application;
- Sealed Secrets passa a ser opcional na conexão de um cluster;
- Na listagem de aplicação mostrar o nome do cluster e permitir filtrar por ele;
- Implementado botão na listagem de cluster/aplicação para mostrar o erro na criação de ambos;
- Corrigido o erro de commit no editor de código;
- Trazer sincronização das aplicações no dashboard;

## V1.2.1
- Criação de aplicações por meio de Terraform;
- Correção de bugs e pequenos ajustes;

## V1.2.0
- Novo credencial cloud: GCP;
- Renomeação de "Projeto" para "Workspace";
- Criação de Workspace padrão na criação de organização;
- Filtros de ordenação para clusters e aplicações;
- Atalho de deploy após realização de um commit (cluster/aplicação);
- Editor de código com commits de múltiplos arquivos;
- Botão de redeploy;
- Comportamento do drawer lateral atualizado;
- Exibição de eventos dos subrecursos do cluster;

## V1.1.0
- Botão de refresh de aplicação
- Exibição de eventos do cluster no ArgoCD;
- Ajustes na deleção de cluster criado pelo Planktron;
- Ajustes na exibição de erros;
- Ajustes no scroll e loading dos detalhes do cluster;
- Exibição de endpoints/portas dos Services/Ingress da aplicação;
- Criação de aplicação por templates;
- Exibição de eventos dos subrecursos da aplicação;
- CLI: Comando para realizar Port-Forward;
- Ajustes de segurança no ArgoCD;
- Edição da aplicação pai (em aplicações componentes);
- Nova opção de login: Azure AD;
- Exclusão de recursos da aplicação no ArgoCD;
- Botão para sincronizar aplicação;
- Ajustes na exibição do README da aplicação;
- Exibição da credencial atual na edição de credencial Opaque;
- Uso do enter para enviar formulários.

## V1.0.2

- Exibição dos status dos recursos do cluster;
- Exibição do status de sincronização da aplicação;
- Exibição dos eventos da aplicação no ArgoCD;
- Edição de aplicação;
- Novo Provider Git: Bitbucket;
- Novo Provider Git: Code Commit;
- Ajustes nos drawers;
- Ajustes no editor de código.

## V1.0.1

- Novo Provider Git: Azure DevOps;
- Edição de credenciais;
- Ferramenta de Apply no cluster;
- Novas etapas no deploy de aplicação e cluster entre a criação e o deploy de edição dos arquivos de deploy.

## V1.0.0

- Login social: Google e Github;
- Login local (e-mail e senha);
- Separação de usuário por níveis de permissão (Owner, Maintainer, etc);
- Gerenciamento de usuários por organização;
- Gerenciamento de grupos de usuários por organização;
- Gerenciamento de projetos;
- Acesso a projetos por usuário/grupo;
- Criação de tokens de sessão para uso externo (CLI);

<p></p>

- Gerenciamento de credenciais;
- Credenciais Git: Github e Gitlab;
- Credenciais Cloud: AWS;
- Credencial de cluster (Kubernetes);
- Credenciais de kube-secrets: Opaque e Container Registry;

<p></p>

- Integração com repositórios Git;
- Visualização do repositório de Gitops;

<p></p>

- Conectar cluster existente;
- Criar cluster (ambientes de homologação e produção);
- Gerenciamento de clusters;
- Download kubeconfig;

<p></p>

- Gerenciamento de aplicações;
- Visualização do README.md da aplicação;
- Visualização dos pods e logs;
- Criação de aplicação a partir de um repositório Git conectado;
- Criação de aplicação como componente;
- Criação de aplicação com credenciais de kube-secrets;

<p></p>

- Marketplace: Listagem de templates de aplicação;
- Criação de aplicação a partir de um template;

<p></p>

- Dashboard;
- Documentação;
- Notificações de eventos do sistema;
- Importar YAML/JSON;
- APIs Swagger;
- Geração e download de Logs do sistema;

<p></p>

- CLI: Login;
- CLI: Gerenciamento de projetos;
- CLI: Gerenciamento de credenciais;
- CLI: Gerenciamento de clusters;
- CLI: Gerenciamento de aplicações;
- CLI: Ferramenta de `apply` diretamente no cluster;
