// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
	title: 'Platform',
	tagline: 'Documentação Oficial',
	url: 'https://public-o2b.gitlab.io',
	baseUrl: '/idp_docs',
	onBrokenLinks: 'throw',
	onBrokenMarkdownLinks: 'warn',
	favicon: '/img/favicon.svg',

	customFields: {
		currentVersion: process.env.CURRENT_VERSION
	},

	// GitHub pages deployment config.
	// If you aren't using GitHub pages, you don't need these.
	organizationName: 'raffOps', // Usually your GitHub org/user name.
	projectName: 'platform', // Usually your repo name.

	// Even if you don't use internalization, you can use this field to set useful
	// metadata like html lang. For example, if your site is Chinese, you may want
	// to replace "en" with "zh-Hans".
	i18n: {
		defaultLocale: 'pt',
		locales: ['pt'],
		localeConfigs: {
			pt: {
				htmlLang: 'pt-BR',
			},
		},
	},

	presets: [
		[
			'classic',
			/** @type {import('@docusaurus/preset-classic').Options} */
			({
				docs: {
					sidebarPath: require.resolve('./sidebars.js'),
					lastVersion: 'current',
					routeBasePath: '/',
				},
				blog: false,
				theme: {
					customCss: require.resolve('./src/css/custom.css'),
				},
			}),
		],
		[
			'redocusaurus',
			{
				specs: [
					{
						route: "/api/auth/",
						spec: "./swagger/auth_swagger.yml",
					},
					{
						route: "/api/idp/",
						spec: "./swagger/api_swagger.yml",
					},
					{
						route: "/api/marketplace/",
						spec: "./swagger/marketplace_swagger.yml",
					},
				],
			},
		],
	],

	themeConfig:
		/** @type {import('@docusaurus/preset-classic').ThemeConfig} */
		({
			colorMode: {
				defaultMode: 'dark',
				disableSwitch: true,
				respectPrefersColorScheme: false,
			},
			navbar: {
				title: '',
				logo: {
					alt: 'Planktron Logo',
					src: '/img/logo_colorido.svg',
					srcDark: '/img/logo_negativo.svg',
				},
				items: [
					{
						type: "doc",
						docId: "intro",
						position: "left",
						label: "Documentação",
					},
					{
						to: '/changelog',
						position: 'left',
						label: 'Changelog',
					},
					{
						type: 'dropdown',
						label: 'APIs Swagger',
						position: 'left',
						items: [
							{ to: '/api/auth', label: 'Auth API' },
							{ to: '/api/idp', label: 'Platform API' },
							{
								to: '/api/marketplace',
								label: 'Marketplace API',
							},
						],
					},
					{
						href: 'https://portal-dev.o2b.io/',
						label: 'Platform',
						position: 'right',
					},
				],
			},
			footer: {
				style: 'dark',
				links: [
					{
						title: 'Company',
						items: [
							{
								label: 'Conheça a O2B',
								href: 'https://o2b.com.br/',
							},
							{
								label: 'Trabalhe conosco',
								href: 'https://o2b.gupy.io/',
							},
						],
					},
					{
						title: 'Social',
						items: [
							{
								href: 'https://www.linkedin.com/company/o2b/',
								label: 'Linkedin',
							},
							{ href: 'https://blog.o2b.com.br/', label: 'Blog' },
							{
								href: 'https://o2b.com.br/#contato',
								label: 'Contato',
							},
						],
					},
				],
				copyright: `Copyright © ${new Date().getFullYear()} O2B, Ltda. Built by Platform Team.`,
			},
			prism: {
				theme: lightCodeTheme,
				darkTheme: darkCodeTheme,
			},
			zoom: {
				selector: '.markdown :not(em) > img',
				config: {
					// options you can specify via https://github.com/francoischalifour/medium-zoom#usage
					background: {
						light: 'rgb(255, 255, 255)',
						dark: 'rgb(50, 50, 50)',
					},
				},
			},
		}),

	plugins: [require.resolve('docusaurus-plugin-image-zoom')],
};

module.exports = config;
