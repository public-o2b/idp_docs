openapi: 3.0.0
info:
  title: Planktron - Marketplace (Docs)
  version: 1.0.0
servers:
  - url: http://{{url_app_planktron_marketplace}}
components:
  securitySchemes:
    noauthAuth:
      type: http
      scheme: noauth
    bearerAuth:
      type: http
      scheme: bearer
security:
  - bearerAuth: []
tags:
  - name: Template
  - name: Tag
  - name: Template Revision
paths:
  /api/v1/template:
    post:
      tags:
        - Template
      summary: Create template
      requestBody:
        content:
          application/json:
            schema:
              type: object
              example:
                name: my-template-name
                description: My template description
                public: false
                project_id: my project path or id
                icon_url: https://www.domain.com/icon.png
                host: https://myserver.com
                color: '#010101'
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
    get:
      tags:
        - Template
      summary: List templates
      parameters:
        - name: private
          in: query
          schema:
            type: boolean
          example: 'true'
        - name: name
          in: query
          schema:
            type: string
          example: my-template-name
        - name: status
          in: query
          schema:
            type: string
          description: active|inactive
          example: active
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template/{id}:
    put:
      tags:
        - Template
      summary: Update template
      requestBody:
        content:
          application/json:
            schema:
              type: object
              example:
                name: my-template-name
                description: My template description
                public: true
                host: https://myserver.com
                project_id: my project path or id
                color: '#010101'
                icon_url: https://www.domain.com/icon.png
                recommended_revision_id: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
    delete:
      tags:
        - Template
      summary: Delete template
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
    get:
      tags:
        - Template
      summary: Get template by ID
      security:
        - noauthAuth: []
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template/{id}/publish:
    put:
      tags:
        - Template
      summary: Publish template
      requestBody:
        content: {}
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template/{id}/tags:
    post:
      tags:
        - Template
      summary: Add tags to template
      requestBody:
        content:
          application/json:
            schema:
              type: object
              example:
                tags:
                  - my-tag-name
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
    delete:
      tags:
        - Template
      summary: Remove tags from template
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template/name/{name}:
    get:
      tags:
        - Template
      summary: Get template by name
      security:
        - noauthAuth: []
      parameters:
        - name: name
          in: path
          schema:
            type: string
          required: true
          description: Template name
          example: my-template-name
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/tag:
    post:
      tags:
        - Tag
      summary: Create tag
      requestBody:
        content:
          application/json:
            schema:
              type: object
              example:
                name: my-tag-name
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
    get:
      tags:
        - Tag
      summary: List tags
      parameters:
        - name: name
          in: query
          schema:
            type: string
          example: my-tag-name
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/tag/{name}:
    get:
      tags:
        - Tag
      summary: Get tag by name
      security:
        - noauthAuth: []
      parameters:
        - name: name
          in: path
          schema:
            type: string
          required: true
          description: Tag name
          example: my-tag-name
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template_revision:
    post:
      tags:
        - Template Revision
      summary: Create template revision
      requestBody:
        content:
          application/json:
            schema:
              type: object
              example:
                name: v1.0.0
                revision: main
                template_id: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
                description: My template description
                public: false
                files:
                  - name: Chart.yaml
                    path: my-folder/v1.0.0
                  - name: values.yaml
                    path: my-folder/v1.0.0
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template_revision/{id}:
    put:
      tags:
        - Template Revision
      summary: Update template revision
      requestBody:
        content:
          application/json:
            schema:
              type: object
              example:
                name: main
                revision: main
                description: Main branch
                public: true
                files:
                  - name: test.yaml
                    path: ''
                  - name: secret.yaml
                    path: ''
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
    delete:
      tags:
        - Template Revision
      summary: Delete template revision
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
    get:
      tags:
        - Template Revision
      summary: Get template revision by ID
      security:
        - noauthAuth: []
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template/{id}/template_revision:
    get:
      tags:
        - Template Revision
      summary: List template revisions
      parameters:
        - name: private
          in: query
          schema:
            type: boolean
          example: 'true'
        - name: limit
          in: query
          schema:
            type: integer
          example: '10'
        - name: actual_page
          in: query
          schema:
            type: integer
          example: '1'
        - name: name
          in: query
          schema:
            type: string
          example: revision-name
        - name: status
          in: query
          schema:
            type: string
          description: active|inactive
          example: active
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template/{id}/template_revision/recommended:
    get:
      tags:
        - Template Revision
      summary: Get recommended revision by template ID
      security:
        - noauthAuth: []
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
          description: Template ID
          example: 8ffd7147-ef00-4faf-b0c4-3ffd702ce1d2
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}
  /api/v1/template/name/{name}/template_revision/recommended:
    get:
      tags:
        - Template Revision
      summary: Get recommended revision by template name
      security:
        - noauthAuth: []
      parameters:
        - name: name
          in: path
          schema:
            type: string
          required: true
          description: Template name
          example: my-template-name
      responses:
        '200':
          description: Successful response
          content:
            application/json: {}

