FROM node:16.19-alpine3.16 AS deps

WORKDIR /usr/src/app

COPY package.json yarn.lock /usr/src/app/
RUN [ -f yarn.lock ] && yarn --frozen-lockfile

FROM node:16.19-alpine3.16 AS builder

ARG ENVIRONMENT

WORKDIR /usr/src/app

COPY --from=deps /usr/src/app/node_modules ./node_modules
COPY . .

RUN yarn build

EXPOSE 3000

ENTRYPOINT npm run serve