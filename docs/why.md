---
title: Por que IDP-O2B?
sidebar_position: 2
---

# Por que executar no IDP-O2B?

Em vez de restringir os arquitetos a um provedor de Cloud, o IDP-O2B permite que os arquitetos e engenheiros de plataforma criem uma combinação resiliente e fácil de usar e repetível de recursos em nuvem.

### Diferenciais do IDP-O2B incluem:

Computação híbrida, multiregião e multicloud: Com IDP-O2B, suas cargas de trabalho podem ser distribuídas e executadas de forma independente em qualquer combinação geográfica e de provedores de nuvem (AWS, Azure, GCP ou outra nuvem pública e privada). Você também pode adicionar os seus clusters do Kubernetes ao IDP-O2B independente de onde foram hospedados. Isso permite alternar nuvens ou adicionar nuvens e recursos com apenas alguns cliques ou ainda replicar toda sua cloud para outra região em minutos.

##### Evite:

- Planos complexos de DR;
- Indisponibilidades regionais;
- Infraestrutura de solução não replicável;
- Migrações lentas;
- E muito mais.

**Backup da nuvem: **Os ambientes criados via IDP-O2B podem ser facilmente replicados entre provedores ou regiões.

**Alinhado com o DevOps:** O IDP-O2B integra os melhores componentes da pilha de operações nativa da nuvem para métricas, registro, gerenciamento de segredos e muito mais. Você também pode integrar facilmente as ferramentas de sua escolha via nosso completo catalogo de soluções opensource amplamente difundidas na atualidade.

**Controle de acesso:** O IDP-O2B fornece controles de autorização avançados de nível empresarial.

**Auditoria:** Conta com auditoria para as ações do IDP-O2B que você executa. Os dados de auditoria são armazernados e podem ser baixados de forma rápida a partir de sua interface de usuário.

## Conclusão

A O2B também oferece opções de suporte e consultoria para ajudar os usuários a criar e gerenciar sua infraestrutura, processos de CI/CD e a completa criação de uma API de implementação para sua solução chegar ao próximo nível e ser utilizada em todo o mundo.

A integração com DevSecOps também é uma característica importante, permitindo aos usuários incorporar práticas de segurança e compliance em seus processos de entrega contínua.

Permite que seu time se concentre no desenvolvimento de suas aplicações, enquanto o gerenciamento da infraestrutura e do software é deixado nas mãos do IDP-O2B.

Por fim, também possui um blog com artigos e tutoriais sobre GitOps, Continuous Delivery, infraestrutura como código e outros assuntos relacionados. O IDP-O2B é uma plataforma flexível e escalável, projetada para atender às necessidades de equipes de desenvolvimento de todos os tamanhos. Ele ajuda a automatizar o processo de entrega de aplicações e infraestrutura, garantindo que as equipes possam se concentrar no desenvolvimento de suas aplicações, enquanto o gerenciamento da infraestrutura e do software é deixado nas mãos do IDP-O2B.
