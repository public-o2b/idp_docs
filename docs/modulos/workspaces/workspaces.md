---
slug: /modules/workspaces
sidebar_position: 2
---

# Workspaces

Workspaces se utilizam dos recursos externos conectados ([Credenciais](/modules/credentials), [Repositórios](/modules/repositories) e [Clusters](/modules/clusters)) para criar um ambiente que represente um contexto específico. Os recursos podem ser compartilhados entre workspaces, entretanto as aplicações são específicas por workspace.

Assim como no caso de organização, o usuário pode selecionar o workspace que define o contexto de modificação atual.

![Listagem de Workspaces](./select_workspace.png)

Caso o nível de acesso do usuário na organização não permita com que ele interaja com todos os workspaces, ele deverá ser adicionado como membro (seja individualmente ou por meio de um grupo) nos workspaces relacionados a sua atuação na organização.

![Membros do workspace](./project-member-list.png)
