---
slug: /modules/repositories
sidebar_position: 4
---

# Repositórios

Após ter configurado suas [Credenciais Git](/modules/credentials#git) é o momento de conectar os repositórios que você utilizará para criar aplicações. Mesmo tendo permissão a todos os repositórios da conta por meio do Access Token, seguimos com este modelo para ser manipulado apenas aqueles cadastrados no IDP-O2B.

![Criar Repositório](./connect_repo.png)

### Repositório de Gitops

O repositório de Gitops é um repositório especial que agrega todas as configurações de infraestrutura da organização, servindo como fonte da verdade do estado atual da mesma. Por conta disso, é permitido que apenas 1 credencial de Git seja escolhida como a que armazenará o repositório de Gitops por organização. Conforme é criado novos clusters e aplicações, o IDP-O2B alimentará o repositório com os dados de infraestrutura.

![Repositório Gitops](./gitops.png)

:::danger Atenção
Cuidado ao alterar o conteúdo do repositório de Gitops manualmente.
O IDP-O2B utiliza-se dele como fonte do estado atual da infraestrutura, então qualquer modificação incorreta poderá acarretar em um mal funcionamento de alguma parte do sistema.
:::
