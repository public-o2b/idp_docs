---
slug: /modules/applications
sidebar_position: 7
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import Repositorio from './criacao/repositorio/_repositorio.mdx';
import Marketplace from './criacao/marketplace/_marketplace.mdx';
import Arquivo from './criacao/arquivo/_arquivo.mdx';

# Aplicações

Para trabalharmos com aplicações no IDP-O2B, primeiramente devemos conectar/criar um cluster, onde a aplicação será disponibilizada. Para mais informações, acesse a seção [Clusters](/modules/clusters).

### Criar uma aplicação

Você poderá criar uma aplicação de três formas diferentes:

A partir de um repositório GIT: se você já tem um repositório conectado ao IDP-O2B, pode usá-lo para criar sua aplicação. Basta preencher o formulário com as informações necessárias e selecionar o repositório desejado.

A partir do marketplace da plataforma: você também pode escolher uma aplicação pré-configurada a partir do nosso marketplace. Selecionando uma aplicação, você terá acesso a uma página com informações e instruções de como configurá-la. Também terá acesso ao yml antes realizar o deploy da aplicação acessando a tela de listagem e utilizando o editor de código.

A partir de um arquivo: você pode importar um arquivo YAML contendo as informações da sua aplicação. O arquivo YAML deve estar no formato correto e seguir as especificações exigidas pela plataforma. Após selecionar o arquivo, o seu conteúdo será exibido no formulário de cadastro e o mesmo poderá ser editado caso haja necessidade.

### Vamos lá!

<Tabs>
  <TabItem value='repositorio' label='A partir de um repositório git' default>
    <Repositorio />
  </TabItem>
  <TabItem value='marketplace' label='A partir de um template'>
    <Marketplace />
  </TabItem>
  <TabItem value='arquivo' label='A partir de um arquivo'>
    <Arquivo />
  </TabItem>
</Tabs>

### Editar uma aplicação

Após criar a aplicação, esta ficará em um estado de espera. Neste momento é possível editar todos os arquivos para customizar do seu modo.

:::info
Para ter acesso a esta tela, basta ir na listagem de aplicações, coluna ações, e clicar no ícone **Editar código dos arquivos**

![Edição dos arquivos](./editor_codigo_aplicacao.png)
:::

:::info
Apenas após a confirmação que o deploy da aplicação será iniciado.
![Aguardando deploy](./deploy-app.png)
:::

:::info
Após o deploy ter sido completo e a aplicação estiver no ar, é possível editar as secrets, o namespace, as políticas de sincronização, as regras de ignore e a aplicação pai que esta utilizará. Para isso, basta clicar no ícone **Editar** na coluna **ações**.
![Editar aplicação](./editar_app.png)
:::

### Excluir uma aplicação

:::danger Atenção
Para excluir uma aplicação, encontre-a na listagem das aplicações dentro da plataforma. Ao encontrá-la, clique no ícone em formato de lixeira na coluna ações, conforme a imagem abaixo.
![Clique no icone da lixeira](./del_app.png)

Após isso, o sistema irá exibir um diálogo requisitando a confirmação da exclusão. Para dar continuidade ao processo, digite o nome da aplicação no campo indicado e clique no botão confirmar para dar início ao processo de remoção da aplicação no cluster.
:::

:::info INFO
Há dois campos que devemos ter bastante atenção antes de confirmar a exclusão da aplicação. O primeiro é o campo: "Excluir recursos de forma recursiva", onde você deverá marcar a opção para efetivar o modo "Cascade". O segundo campo é o "Excluir arquivos do repositório de GitOps", onde você deverá marcar a opção para efetivar o modo "RemoveFile".

### Descrição dos modos

Excluir recursos de forma recursiva - Esta opção é usada quando se deseja deletar um cluster e todos os objetos dependentes dele, como tabelas, índices, sequências, etc. Isso garante que todos os objetos relacionados ao cluster sejam deletados automaticamente.

Excluir arquivos do repositório de GitOps - Esta opção é usada quando se deseja excluir os arquivos do repositório de GitOps. Ao marcar esta opção, os arquivos do cluster serão removidos do repositório. Caso não marque esta opção, os arquivos continuarão em seu repositório de GitOps, mas não serão mais utilizados.

![Clique no icone da lixeira](./del_app2.png)
:::

### Árvore de recursos da aplicação

:::info INFO
A tela de recursos apresenta um fluxo em formato de árvore que representa os diferentes recursos disponíveis na aplicação. Cada nó da árvore representa um recurso específico. Além disso, a tela fornece informações de sincronização e saúde da aplicação, permitindo que os usuários monitorem o desempenho e a disponibilidade dos recursos.

Os recursos são organizados em diferentes níveis hierárquicos, refletindo a estrutura da aplicação. Cada nó da árvore pode ser clicado para exibir detalhes do código de cada um e suas diferenças.

A seção de sincronização exibe informações sobre o status da sincronização dos dados da aplicação no ArgoCD, permitindo que os usuários identifiquem problemas de integração ou atualização. A seção de saúde fornece informações sobre o desempenho e o consumo de recursos da aplicação.

Em resumo, a tela de recursos é uma ferramenta essencial para monitorar a saúde e a sincronização dos recursos da aplicação. Com seu formato de árvore intuitivo e detalhes abrangentes, os usuários podem facilmente identificar problemas e tomar ações corretivas para garantir o desempenho e a disponibilidade contínuos da aplicação.

Para acessar esta página, basta clicar no ícone **Árvore de recursos** na coluna **Ações** dentro da listagem de aplicações, como é mostrado na image abaixo.

![Recursos tree](./recursos_tree.png)
:::

### Sincronizar aplicação

Caso precise refazer o sync ou sincronizar sua aplicação, basta clicar no ícone de ação na listagem de aplicações. Ele irá **Syncar** a aplicação novamente dentro do ArgoCD.

:::info Segue imagem de exemplo:
![Sync app](./sync_app.png)
:::

### Status da aplicação

Ao interagir com uma aplicação, o IDP-O2B vem a apresentar alguns status conforme ele processa uma solicitação. Essa solicitação pode ser, por exemplo, uma solicitação de criação ou atualização de uma aplicação. A tabela abaixo, lista e descreve o significado de cada status:

<table>
  <thead>
    <tr>
      <th width='20%'>Status</th>
      <th width='40%'>Descrição</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Transferência de arquivos</td>
      <td>
        O IDP-O2B irá começar a transferir os arquivos da aplicação para o
        cluster.
      </td>
    </tr>
    <tr>
      <td>Transferindo arquivos</td>
      <td>
        O IDP-O2B está transferindo os arquivos da aplicação para o cluster.
      </td>
    </tr>
    <tr>
      <td>Ativo</td>
      <td>
        O IDP-O2B finalizou todo o processo de criação da aplicação com
        sucesso.
      </td>
    </tr>
    <tr>
      <td>Inativo</td>
      <td>A aplicação foi desativada.</td>
    </tr>
    <tr>
      <td>Falha</td>
      <td>
        O IDP-O2B tentou executar o processo de criação porém não obteve
        successo.
      </td>
    </tr>
    <tr>
      <td>Aguardando deploy</td>
      <td>
        O IDP-O2B está aguardando o deploy da aplicação por parte do usuário.
      </td>
    </tr>
    <tr>
      <td>Aplicação de recursos</td>
      <td>
        O IDP-O2B irá começar a aplicar os recursos da aplicação no cluster.
      </td>
    </tr>
    <tr>
      <td>Aplicando recursos</td>
      <td>O IDP-O2B está aplicando os recursos da aplicação no cluster.</td>
    </tr>
    <tr>
      <td>Iniciando Atualização</td>
      <td>
        O IDP-O2B irá começar a atualizar a aplicação com as alterações
        realizadas.
      </td>
    </tr>
    <tr>
      <td>Atualizando</td>
      <td>
        O IDP-O2B está atualizando a aplicação com as alterações realizadas.
      </td>
    </tr>
    <tr>
      <td>Iniciando Remoção</td>
      <td>
        O IDP-O2B irá começar a remoção de todas as alterações realizadas
        durante a criação da aplicação.
      </td>
    </tr>
    <tr>
      <td>Removendo</td>
      <td>
        O IDP-O2B está removendo todas as alterações que foram realizadas
        durante a criação da aplicação.
      </td>
    </tr>
  </tbody>
</table>

Além disso, também há os status correspondentes as fases de cada pod (que são listados nos detalhes da aplicação). Estes status são gerenciados diretamente pelo ArgoCD.

![Pods da aplicação](./application-pods.png)

:::info INFO
Ao clicar em detalhes na aplicação, abrirá uma tela onde será possível visualizar o status atual da aplicação e os logs dos pods atualizados.
:::
