---
slug: /modules/clusters
sidebar_position: 5
---

# Clusters

O IDP-O2B permite a conexão com um cluster de duas formas: através da criação de um novo cluster ou conectando a um cluster já existente.

### Criar um Cluster

Atualmente o IDP-O2B tem a capacidade de criar um novo cluster utilizando credenciais da AWS, GCP e Azure. Este serviço permite a criação de um cluster nas respectivas nuvens citadas.

Antes de iniciar o processo, é necessário possuir uma credencial do tipo Cloud cadastrada na plataforma. Caso não possua, basta ir na seção [Credenciais](/modules/credentials#cloud) e selecionar a opção Cloud para saber mais sobre como realizar o cadastro.

Para dar início ao processo, vá a tela de clusters e selecione a opção **Criar cluster**.

![Criar Cluster](./create_cluster.png)

Na janela de criação, será requisitado as seguintes informações:

- Nome do Cluster

  Nome que será utilizado para identificar o cluster no IDP-O2B.

- Credencial de Cloud

  Credencial do tipo cloud que vem a dar acesso a sua conta na AWS, GCP ou Azure e permitir realizar o processo de criação do cluster.

- Tipo de rede

  Junto a criação do cluster também vem a ser criado o serviço Gateway Nat. Esse serviço é responsável por realizar a conversão de endereços de rede possibilitando que instâncias em uma sub-rede possam vir a se conectarem entre si e a serviços fora da [VPC](https://aws.amazon.com/pt/vpc/). Para mais detalhes sobre esse serviço, acesse a [documentação oficial](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html).

  - Homologação: Para o ambiente de homologação será criado um Gateway NAT junto a três subnet públicas e a 3 subnet três privadas.
  - Produção: Para o ambiente de produção será criado três Gateway NAT junto a a três subnet públicas e a 3 subnet três privadas.

:::info Atenção
O campo **tipo de rede** só ficará disponível para credenciais de cloud do tipo AWS.
:::

<div
  style={{
    textAlign: 'center'
  }}
>
  <img
    src={require('./create_cluster_nat.png').default}
    alt="Preencha as informacoes iniciais"
    style={{
      margin: '16px 0px',
      height: '500px'
    }}
  />
</div>

Após preencher as informações necessárias, clique no botão **Criar cluster** para dar início ao processo.

### Conectar um cluster

Ao utilizar esta opção, você poderá conectar o seu cluster de duas formas. A primeira é através de nosso CLI (Interface de Linha de Comando), disponível para download em nossa plataforma. Na segunda opção será exibido um formulário onde você poderá informar o nome e as credenciais necessárias para estabelecer a conexão. Essa facilidade permite uma integração rápida e segura com o nosso serviço, garantindo a agilidade e a eficiência que nossos clientes precisam para gerenciar seus projetos.

Para conectar um cluster já existente ao IDP-O2B, primeiramente é necessário cadastrar uma credencial de um cluster Kubernetes. Caso ainda não possua, vá na seção [Credenciais](/modules/credentials) e selecione a opção Kubernetes Cluster, onde conterá todos os passos necessário para realizar o cadastro.

Para dar início ao processo, vá a tela de clusters e selecione a opção _Conectar cluster_.

**Utilizando o CLI**
:::info Atenção
Aqui basta seguir os steps mostrados no drawer de conexão de cluster.

![Criar Cluster](./connect_cluster_cli.png)
:::

**Utilizando o fomulário**
:::info Atenção
No formulário basta informar o nome do cluster e a sua credencial, que deve ser criada na página de [Credenciais](/modules/credentials)

![Criar Cluster](./connect_cluster_form.png)
:::

### Editar um cluster

Após criar um cluster, este ficará em um estado de espera. Neste momento é possível editar todos os arquivos para customizar do seu modo.

:::info 1º
![Editar cluster](./code-editor-2.png)
:::
:::info 2º
![Editar cluster](./code-editor.png)
:::

### Excluir um Cluster

Ao excluir um cluster do IDP-O2B, será também removido todos os recursos utilizados em sua criação, por isso, essa opção só é habilitada caso o cluster tenha sido criado pela plataforma. Para realizar a exclusão, na tela de clusters, encontre o cluster a ser removido e na coluna ações clique no ícone com o símbolo de lixeira.

![Desativar Cluster](./delete_cluster.png)

O sistema irá exibir um diálogo requisitando a confirmação da exclusão. Para dar continuidade ao processo, digite o nome do cluster no campo indicado e clique no botão confirmar para dar início ao processo de remoção do cluster.

:::info Atenção
Há dois campos que devemos ter bastante atenção antes de confirmar a exclusão do cluster. O primeiro é o campo: "Excluir recursos de forma recursiva", onde você deverá marcar a opção para efetivar o modo "Cascade". O segundo campo é o "Excluir arquivos do repositório de GitOps", onde você deverá marcar a opção para efetivar o modo "RemoveFile".

### Descrição dos modos

Excluir recursos de forma recursiva - Esta opção é usada quando se deseja deletar um cluster e todos os objetos dependentes dele, como tabelas, índices, sequências, etc. Isso garante que todos os objetos relacionados ao cluster sejam deletados automaticamente.

Excluir arquivos do repositório de GitOps - Esta opção é usada quando se deseja excluir os arquivos do repositório de GitOps. Ao marcar esta opção, os arquivos do cluster serão removidos do repositório. Caso não marque esta opção, os arquivos continuarão em seu repositório de GitOps, mas não serão mais utilizados.

:::

![Dialogo de exclusao](./delete_cluster2.png)

### Desativar um Cluster

Ao desativar um cluster ele será removido da plataforma, porém o IDP-O2B não fará a deleção dos recursos do cluster onde ele estiver hospedado. Para isso, na tela de clusters, busque o cluster que deseja remover e na coluna ações clique no ícone **Desativar**, conforme é mostrado na imagem abaixo.

![Desativar Cluster](./disable_cluster.png)

O sistema irá exibir um diálogo requisitando a confirmação da ação de desativar. Para dar continuidade ao processo, digite o nome do cluster no campo indicado e clique no botão confirmar para dar início ao processo da desativação do cluster.

![Dialogo de exclusao](./disable_cluster2.png)

### Terminal (Kubectl) integrado

A feature em questão permite que o usuário abra um Terminal (Kubectl) integrado diretamente ao cluster Kubernetes da lista de clusters exibida. Essa funcionalidade é útil para realizar operações diretamente no cluster sem precisar sair da aplicação ou abrir uma janela do terminal separadamente.

Ao clicar no botão de "Abrir Terminal" na lista de clusters, o usuário será direcionado para um terminal integrado à aplicação. Esse terminal é conectado diretamente ao cluster selecionado, permitindo ao usuário executar comandos do Kubectl.

O terminal integrado suporta todos os comandos do Kubectl, incluindo operações de gerenciamento de pods, deployment, configurações e muito mais. Isso permite que o usuário possa realizar tarefas diretamente no cluster sem precisar abrir outro terminal ou usar ferramentas de terceiros.

Com essa feature, o usuário tem mais facilidade e agilidade na execução de tarefas no cluster Kubernetes, aumentando a produtividade e simplificando o processo de gerenciamento de clusters.

:::info 1º
![Cluster terminal - 1](./cluster_terminal1.png)
:::

:::info 2º
![Cluster terminal - 2](./cluster_terminal2.png)
:::

### Baixar Kubeconfig

Caso seja necessário interagir com o cluster criado pelo IDP-O2B, você poderá utilizar a ferramenta kubectl. O [kubectl](https://kubernetes.io/docs/reference/kubectl/) é uma ferramenta de linha de comando que possibilita a utilização da API do kubernetes. Em sua configuração, é utilizado o arquivo chamado kubeconfig, nele conterá todas as informação de conexão ao cluster.

Para facilitar a configuração da ferramenta, o IDP-O2B tem a funcionalidade de gerar automaticamente esse arquivo. O seu download pode ser feito através da coluna ações, referente ao cluster que deseja-se conectar. Depois clique no ícone **Baixar Kubeconfig**. O download do arquivo será iniciado automaticamente. Para mais detalhes sobre o kubeconfig, visite a [documentação oficial](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/).

:::info Atenção
O download do arquivo do **Kubeconfig** só estará disponível para clusters que foram criados pela plataforma. Caso o cluster seja conectado ao IDP-O2B, a opção estará desabilitada.
:::
![Baixar kubeconfig](./download_kubeconfig.png)

### Cluster Status

Ao interagir com um cluster, o IDP-O2B vem a apresentar alguns status conforme ele processa uma solicitação. Essa solicitação pode ser, por exemplo, uma solicitação de criação de um cluster. A tabela abaixo, lista e descreve o significado de cada status:

<table>
  <thead>
    <tr>
      <th width="20%">Status</th>
      <th width="40%">Descrição</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Transferência de arquivos</td>
      <td>O IDP-O2B recebeu uma solicitação e em breve dará início ao processo de transferência de arquivos.</td>
    </tr>
    <tr>
      <td>Transferindo arquivos</td>
      <td>O sistema está realizando a montagem dos arquivos correpondentes ao cluster com base no repositório escolhido.</td>
    </tr>
    <tr>
      <td>Aguardando deploy</td>
      <td>
        Quando o cluster estiver com status "Aguardando deploy", o usuário deverá clicar na ação de "Deploy" da tabela para o processo continuar.
        O sistema montou os arquivos e está aguardando a edição dos mesmos por parte do usuário e a confirmação de deploy.
      </td>
    </tr>
	<tr>
		<td>Aplicação de recursos</td>
		<td>A confirmação de deploy foi recebida e o processo de deploy foi iniciado.</td>
	</tr>
	<tr>
		<td>Aplicando recursos</td>
		<td>O IDP-O2B está criando/atualizando os recursos do cluster com base na solicitação realizada.</td>
	</tr>
		<tr>
		<td>Ativo</td>
		<td>O processo foi finalizado e o cluster está ativo e no ar.</td>
	</tr>
    <tr>
      <td>Iniciando remoção</td>
      <td>A solicitação de remoção foi recebida e está sendo inicializada.</td>
    </tr>
	<tr>
      <td>Removendo</td>
      <td>O IDP-O2B está realizando a remoção dos recursos do cluster por completo.</td>
    </tr>
	<tr>
      <td>Iniciando conexão</td>
      <td>O IDP-O2B está se conectando ao cluster.</td>
    </tr>
	<tr>
      <td>Adicionando componentes</td>
      <td>O IDP-O2B está criando os componentes necessários no cluster para o correto funcionamento da plataforma</td>
    </tr>
	<tr>
      <td>Inativo</td>
      <td>O ciclo do cluster foi finalizado com sucesso e o mesmo não se encontra mais ativo.</td>
    </tr>
    <tr>
      <td>Falha</td>
      <td>O ciclo do cluster foi interrompido por um erro e finalizado com falha. Desta forma ele veio a remover qualquer alteração realizada, porém o estado do cluster não pode mais ser assegurado.</td>
    </tr>  
  </tbody>
</table>

:::info INFO
Ao clicar no cluster, abrirá uma tela de detalhes, onde será possível visualizar alguns detalhes, como mostra a imagem a seguir.
:::

![Cluster detalhes](./cluster-details.png)
