Para poder conectar um cluster existente ao IDP-O2B, você precisará criar uma credencial de cluster.

1. Você irá precisar do CLI kubectl instalado e configurado com seu cluster. Confira a documentação do **[kubectl aqui](https://kubernetes.io/docs/tasks/tools/)**.

2. Crie uma Service Account para obter o token de acesso ao cluster. Para isso, utilize o comando abaixo:

```bash
kubectl apply -f https://raw.githubusercontent.com/IDP-O2B/Quick-start/main/service_account.yaml
```

3. Extraia o token de acesso ao seu cluster através da Service Account criada no passo anterior:

```bash
export TOKENNAME=$(kubectl -n kube-system get serviceaccount/IDP-O2B-manager -o jsonpath='{.secrets[0].name}')
export TOKEN=$(kubectl -n kube-system get secret $TOKENNAME -o jsonpath='{.data.token}' | base64 --decode)
```

4. Crie uma Credencial de cluster no IDP-O2B:

   1. Selecione um workspace no IDP-O2B
   2. Na barra lateral esquerda, clique em **Credenciais**, depois em **Criar credencial**. Então irá abrir uma barra de formulário na lateral direita
   3. Em provedor, selecione **Kubernetes Cluster**
   4. Informe um identificador para a credencial, a URL e o token do cluster
   5. Clique em **Confirmar**

   ![Credenciais - Kubernetes Cluster](./cluster_credential.png)

5. Conecte o cluster ao IDP-O2B:

   1. Ao acessar o item **Clusters** no menu lateral, você deverá clicar em **Conectar cluster**.

   ![Cluster - Conectar Cluster](./connect_cluster.png)

   2. Ao utilizar esta opção, você poderá conectar o seu cluster de duas formas. A primeira é através de nosso CLI (Interface de Linha de Comando), disponível para download em nossa plataforma. Na segunda opção será exibido um formulário onde você poderá informar o nome e as credenciais necessárias para estabelecer a conexão. Essa facilidade permite uma integração rápida e segura com o nosso serviço, garantindo a agilidade e a eficiência que nossos clientes precisam para gerenciar seus projetos."
   <!-- 2. Informe o nome do cluster e a credencial de Kuberenetes Cluster que você criou no passo anteriror -->

   **CLI**
   ![Cluster - Formulário](./connect_cluster_cli.png)

   **CREDENCIAIS**
   ![Cluster - Formulário](./connect_cluster_form.png)

   3. Por fim, clique em **Conectar Cluster**
