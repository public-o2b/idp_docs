Para poder criar um novo cluster, você precisará criar uma Credencial de Cloud (AWS) e selecioná-la na criação de um novo cluster no IDP-O2B.

1. Crie uma Credencial de Cloud (AWS):

   Aqui você poderá criar e gerenciar as credenciais necessárias para integrar a sua conta AWS com nosso serviço. Para começar, basta preencher os campos do formulário com as informações solicitadas. É importante lembrar que as credenciais geradas aqui são criptografadas e armazenadas de forma segura, garantindo a privacidade e a segurança dos seus dados.

   1. Selecione um workspace no IDP-O2B
   2. Na barra lateral esquerda, clique em **Credenciais**, depois em **Criar credencial**. Então irá abrir uma barra de formulário na lateral direita
   3. Em provedor, selecione **AWS**
   4. Informe um identificador para a credencial, a access key e secret key

   ![Credenciais - AWS](./cloud_credential.png)

2. Crie um novo cluster:

   1. No menu lateral, clique em **Clusters**. Após isso, clique em **Criar Cluster**.

   ![Cluster - Criar Cluster](./create_cluster.png)

   2. Informe o nome do cluster e a credencial de Cloud que você criou no passo anteriror

   ![Cluster - Formulário](./create_cluster_2.png)

   3. Por fim, clique em **Criar Cluster**
