---
slug: /quick_start/gitops_credential
sidebar_position: 1
---

# 1. Crie seu repositório de GitOps

Para poder seu repositório de de GitOps, você precisará criar uma credencial de repositório Git.

1. Crie uma Credencial de Repositório no IDP-O2B:

    1. Selecione um workspace no IDP-O2B

    2. Na barra lateral esquerda, clique em **Credenciais**, depois em **Criar credencial**. Então irá abrir uma barra de formulário na lateral direita

    3. Em provedor, selecione **Github**, **Gitlab** ou **Azure DevOps**

    4. Informe um identificador para a credencial
    
    5. Se optar por Gitlab será necessário informar a URL do seu servidor de Gitlab. Caso opte por Github ou Azure DevOps, a URL será preenchida automaticamente

    6. Preencha o nome de usuário e email

    7. O token de acesso você deve gerar no seu provedor e preencher no formulário. Você pode obter ajuda sobre como gerar o token na documentação do seu provedor (**[Gitlab Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)** | **[Github Personal Access Token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)** | **[Azure DevOps Personal Access Token](https://learn.microsoft.com/en-us/azure/devops/organizations/accounts/use-personal-access-tokens-to-authenticate?view=azure-devops&tabs=Windows)**)

    :::caution Atente-se
        O token deve ter permissão de leitura e escrita em repositórios/workspaces
    :::

    8. Marque a opção **Usar esta credencial como GitOps**

    9. Clique em **Confirmar**

    ![Credenciais - Repositório](./repository_credential.png)