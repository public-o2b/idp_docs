---
slug: /quick_start/deploy_your_first_app
sidebar_position: 5
---

# 5. Implante sua primeira aplicação

Aqui você pode criar uma aplicação de três formas diferentes:

A partir de um repositório GIT: se você já tem um repositório conectado ao IDP-O2B, pode usá-lo para criar sua aplicação. Basta preencher o formulário com as informações necessárias e selecionar o repositório desejado.

A partir do marketplace da plataforma: você também pode escolher uma aplicação pré-configurada a partir do nosso marketplace. Selecionando uma aplicação, você terá acesso a uma página com informações e instruções de como configurá-la. Também terá acesso ao yml antes realizar o deploy da aplicação acessando a tela de listagem e utilizando o editor de código.

A partir de um arquivo: você pode importar um arquivo YAML contendo as informações da sua aplicação. O arquivo YAML deve estar no formato correto e seguir as especificações exigidas pela plataforma. Após selecionar o arquivo, o seu conteúdo será exibido no formulário de cadastro e o mesmo poderá ser editado caso haja necessidade.

![Nova Applicação](./creat_app.png)

Escolha uma das opções acima para começar a criar a sua aplicação. Vamos lá!

**Criar aplicação a partir de um repositório GIT**:

<!-- ![Nova Applicação](./create_application.png) -->

1.  Informe um nome para a sua aplicação

2.  Selecione o tipo (Kubernetes, Terraform ou Crossplane)

3.  Selecione as secrets que deseja inserir (Poderá selecionar múltiplas secrets)

    :::info INFO
    Campo apenas para aplicações do tipo **Kubernetes**
    :::

4.  Selecione a sua credencial de cloud criada na página de Credenciais.

    :::info INFO
    Campo apenas para aplicações do tipo **Terraform ou Crossplane**
    :::

5.  Selecione a Região

    :::info INFO
    Campo apenas para aplicações do tipo **Terraform**
    :::

6.  Selecione o cluster de destino da sua aplicação

    :::info INFO
    Campo apenas para aplicações do tipo **Kubernetes**
    :::

7.  Informe o namespace de destino dentro do cluster

    :::info INFO
    Campo apenas para aplicações do tipo **Kubernetes**
    :::

8.  Defina as **Políticas de sincronização** que serão configuradas em sua aplicação.

    :::info INFO
    Sync Automático: Refere-se à sincronização automática de recursos da aplicação com a fonte de configuração, garantindo que a configuração atual seja refletida na implantação. Isso pode ser feito por meio de gatilhos, onde as alterações em uma determinada configuração disparam a sincronização.

    Allow Empty: Refere-se à opção que permite a criação de uma manifestação (ou arquivo de configuração) sem nenhum recurso definido. Isso pode ser útil em situações em que o usuário deseja criar uma configuração inicial com alguns recursos ausentes e adicioná-los posteriormente.

    Prune: Refere-se à opção que permite que o Argo CD exclua recursos não mais necessários que não estão mais presentes na fonte de configuração. Isso ajuda a manter a consistência entre a fonte de configuração e a implantação, removendo recursos obsoletos.

    Self Heal: Refere-se à capacidade de um sistema de monitorar constantemente sua própria integridade e corrigir automaticamente problemas detectados. No contexto do Argo CD, isso significa que a plataforma pode monitorar a implantação e corrigir automaticamente problemas de configuração, mantendo a integridade do sistema. Isso pode incluir a reconstrução de recursos que falharam, a reconfiguração de recursos com erros e a aplicação de patches de segurança.
    :::

9.  Selecione o repositório onde se localizam os arquivos .YAML de deploy da sua aplicação

10. Selecione a Branch

11. Selecione o path de origem dos arquivos

12. Clique em **Confirmar**

![Nova Applicação](./create_application_2.png)
