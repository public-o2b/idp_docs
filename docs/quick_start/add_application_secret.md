---
slug: /quick_start/add_application_secret
sidebar_position: 4
---

# 4. Adicione uma Secret

Para criarmos uma Secret que pode ser utilizada em uma aplicação/componente, você precisará criar uma credencial de Secret.

1. Crie uma Credencial de Secret no IDP-O2B:

    1. Selecione um workspace no IDP-O2B

    2. Na barra lateral esquerda, clique em **Credenciais**, depois em **Criar credencial**. Então irá abrir uma barra de formulário na lateral direita

    3. Em provedor, selecione **Opaque** ou **Docker Registry** (em breve as demais secrets serão implementadas)

    4. Preencha os dados da Secret em **Detalhes da credencial**

    5. Clique em **Confirmar**

    ![Credenciais - Secret](./app_secret_opaque.png)