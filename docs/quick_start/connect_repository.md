---
slug: /quick_start/connect_respository
sidebar_position: 3
---

# 3. Conecte seu repositório

Para poder conectar um repositório, você precisará criar uma credencial de repositório Git e aplicá-la na criação de um repositório no IDP-O2B.

1. Crie um Repositório no IDP-O2B:

   1. Selecione um workspace no IDP-O2B

   2. Na barra lateral esquerda, clique em **Repositórios**, depois em **Criar repositório**. Então irá abrir uma barra de formulário na lateral direita

   3. Informe o nome, a credencial de repositório que você criou no passo anteriror e, na lista de repositórios, selecione o repositório desejado.

   4. Clique em **Criar repositório**

   ![Credenciais - Repositório](./connect_repo.png)
