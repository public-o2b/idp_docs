---
sidebar_position: 1
---

# Introdução

O CLI é uma interface de linha de comando que agrega as principais funcionalidades da interface web porém com uma usabilidade mais próxima do dia-a-dia do Dev.