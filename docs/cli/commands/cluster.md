---
slug: /cli/commands/cluster
sidebar-position: 5
---

# Clusters

### Listar clusters

```bash
idp cluster list
```

### Visualizar cluster

```bash 
idp cluster get --cluster {clusterName}
```

### Criar cluster

```bash 
idp cluster create --name {name} --repository-credential-id {repository-credential-id} --cloud-credential-id {cloud-credential-id}
```

### Excluir cluster

```bash 
idp cluster delete --cluster {clusterName}
```

### Conectar cluster

```bash 
idp cluster connect
```
Para não utilizar o Sealed Secrets você pode adicionar a flag **--use-sealed-secrets false** (default true)

### Desconectar cluster

```bash 
idp cluster disconnect --cluster {clusterName}
```

### Obter kubeconfig do cluster

```bash 
idp cluster get-kubeconfig --cluster {clusterName}
```