---
slug: /cli/commands/credentials
sidebar-position: 4
---

# Credenciais

### Listar credenciais

```bash
idp credentials list
```
