---
slug: /cli/commands/application
sidebar-position: 5
---

# Aplicacões

### Listar aplicacões

```bash
idp application list
```

### Visualizar eventos de uma aplicacão

```bash 
idp application events --workspace {workspaceName} --app {applicationName}
```

### Visualizar uma aplicacão

```bash 
idp application get --workspace {workspaceName} --app {applicationName}
```

### Visualizar pods de uma aplicacão

```bash 
idp application pods --workspace {workspaceName} --app {applicationName}
```

### Visualizar logs de pod de uma aplicacão

```bash 
idp application pod-logs --workspace {workspaceName} --app {applicationName} --pod-name {pod-name}
```

### Fazer Port-Forward em pod de uma aplicação

```bash
idp application port-forward --workspace {workspaceName} --app {applicationName} --pod-name {pod-name} --pod-port {pod-port} --local-port {local-port}
```