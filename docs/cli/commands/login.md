---
slug: /cli/commands/login
sidebar-position: 2
---

# Login

### Configurar token

```bash
idp login set --access-key {access_key} --secret-key {secret-key}
```

### Verificar login atual

```bash
idp login show-logged 
```