---
slug: /cli/commands/general
sidebar-position: 1
---

# Global

### Atualizar o CLI para a última versão disponível

```bash
idp update
```

### Checar versão

```bash
idp --version, -v
```

### Ajuda

```bash
idp help, --help, -h
```