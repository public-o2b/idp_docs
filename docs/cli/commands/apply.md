---
slug: /cli/commands/apply
sidebar-position: 4
---

# Apply

### Para aplicar recursos a partir de um arquivo .YAML/.JSON

```bash
idp apply -f ./resources/test.yaml
```
### ou

```bash
idp apply -f https://raw.githubusercontent.com/{org}/{repoName}/{branch}/{filename}.yaml
```

### Para aplicar recursos a partir de diretório com arquivos .YAML/.JSON

```bash
idp apply -k ./resources/
```

# Exemplo de estruturas YAML

### Workspace
```yaml
apiVersion: v1
kind: Project
metadata:
    name: my-project
    description: Foo Bar Baz
```

### Credencial de Git
```yaml
apiVersion: v1
kind: ProviderAccount
metadata:
    provider: Github
    name: my-git-account
    username: {{myUsername}}
    password: {{myPersonalToken}}
    serverUrl: https://api.github.com #https://yourGitlabServer.com
    email: {{myEmailAddress}}
    gitopsAccount: true
    gitopsRepoName: {{myGitOpsRepositoryName}}
    project: my-project
```

### Credencial de AWS
```yaml
apiVersion: v1
kind: ProviderAccount
metadata:
    provider: AWS
    name: my-aws-account
    username: {{myAccessKey}}
    password: {{mySecretKey}}
    project: my-project
```

### Credencial de Cluster Kubernetes
```yaml
apiVersion: v1
kind: ProviderAccount
metadata:
    provider: Kubernetes Cluster
    name: my-cluster-account
    password: {{myClusterToken}}
    serverUrl: {{myClusterUrl}}
    project: my-project
```

### Conectar um Cluster existente
```yaml
apiVersion: v1
kind: Cluster
metadata:
    name: development
    clusterProviderAccount: my-cluster-account
    operation: Connect
    project: my-project
```

### Criar um novo Cluster EKS (AWS)
```yaml
apiVersion: v1
kind: Cluster
metadata:
    name: development
    cloudProviderAccount: my-aws-account
    natGateway: single #multi
    operation: Create
    project: my-project
```

### Repositório
```yaml
apiVersion: v1
kind: Repository
metadata:
    name: my-git-repository
    url: {{myGitRepoUrl}}
    providerAccount: my-git-account
    project: my-project
```

### Aplicação a partir de um repositório Git
```yaml
apiVersion: v1
kind: Application
metadata:
    name: hello-world-from-git
    project: my-project
    cluster: development
    repository: my-git-repository
    parent: #parent app name
    operation: fromRepository
    options:
        repository:
            revision: main
            path: hello-world
        cluster:
            namespace: my-namespace
        secrets:
            - my-secret
```

### Aplicação a partir de arquivos Helm Chart
```yaml
apiVersion: v1
kind: Application
metadata:
    name: hello-world-from-helm
    project: my-project
    cluster: development
    parent: #parent app name
    operation: fromHelmChart
    options:
        helm:
            #Based 64 Chart.yaml file content
            chart: dGVzdCBjaGFydA==
            #Based 64 values.yaml file content
            values: dGVzdCB2YWx1ZXM=
        cluster:
            namespace: my-namespace
        secrets:
            - my-secret
```

### Aplicação a partir de arquivos Helm Chart
```yaml
apiVersion: v1
kind: Application
metadata:
    name: hello-world-from-template
    project: my-project
    cluster: development
    parent: #parent app name
    operation: fromTemplate
    template: o2b-nginx
    options:
        cluster:
            namespace: my-namespace
        secrets:
            - my-secret
```

### Atualizando uma aplicação (apenas namespace e secrets)
```yaml
apiVersion: v1
kind: Application
metadata:
    name: hello-world
    project: my-project
    options:
        cluster:
            namespace: my-namespace
        secrets:
            - my-secret
```