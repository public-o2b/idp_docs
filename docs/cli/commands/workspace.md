---
slug: /cli/commands/workspace
sidebar-position: 3
---

# Workspaces

### Listar workspaces

```bash
idp workspace list
```

### Visualizar workspace

```bash 
idp workspace get --workspace {workspaceName}
```

### Criar workspace

```bash 
idp workspace create --name {name} --description {description}
```

### Excluir workspace

```bash 
idp workspace delete --workspace {workspaceName}
```