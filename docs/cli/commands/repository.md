---
slug: /cli/commands/repository
sidebar-position: 3
---

# Repositórios

### Listar repositórios

```bash
idp repository list
```

### Visualizar repositório

```bash 
idp repository get --repo {repositoryName}
```

### Criar repositório

```bash 
idp repository create --name {name} --url {repoUrl} --credential {gitCredentialId}
```

### Excluir repositório

```bash 
idp repository delete --repo {repositoryName}
```