### Windows

##### 1. Realize o download do instalador:

###### Windows x64
```bash
https://downloads.o2b.io/cli-setup/windows/x64/idp-installer.exe
```
###### Windows x86
```bash
https://downloads.o2b.io/cli-setup/windows/x86/idp-installer.exe
```

##### 2. Execute o instalador *idp-installer.exe* e siga os passos até finalizar

![Instador do CLI](./cli-windows-installer.png)

##### 3. Adicione o caminho da aplicação ao PATH do sistema operacional
```bash
$ set PATH=%PATH%;C:\Program Files (x86)\O2B\IDP-O2B CLI
```
##### **OU** você pode tentar pela interface do Windows:

    3.1 Em Pesquisar, pesquise e selecione: Sistema (Painel de controle)
    3.2. Clique no link Configurações avançadas do sistema.
    3.3. Clique em Variáveis ​​de ambiente. Na seção Variáveis do Sistema, localize a variável de ambiente  PATH e selecione-a. 
    3.4. Clique em Editar. Se a variável de ambiente PATH não existir, clique em Novo.
    3.5. Na janela Editar Variável do Sistema (ou Nova Variável do Sistema), especifique o valor da variável de ambiente PATH. 
    3.6. Clique OK. 
    3.7. Feche todas as janelas restantes clicando em OK.