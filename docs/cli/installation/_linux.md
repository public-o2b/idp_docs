### Linux

##### 1. Realize o download do instalador:

```bash
$ https://downloads.o2b.io/cli-setup/linux/idp-installer.zip
```

##### 2. Extraia o conteúdo do arquivo idp-installer.zip e na pasta onde encontram-se os arquivos, execute o comando a seguir, para dar permissão ao instalador:

```bash
$ chmod +x install.sh 
```

##### 3. Execute o instalador com o comando:

```bash
$ ./install.sh 
```

##### 4. Ao concluir a instalação, você poderá verificar a versão através do comando:

```bash
$ idp --version
```

##### 5. Será exibido uma mensagem contendo a versão instalada, no exemplo abaixo, é exibido a mensagem para a versão 1.0.1:

```bash
$ idp version 1.0.1
```

#### Caso não seja encontrado o comando idp, adicione o seguinte caminho às variáveis de ambiente:
```bash
  ~/idp
```