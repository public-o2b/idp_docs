FROM node:20

USER node

WORKDIR /usr/src/app
COPY . .
RUN [ -f yarn.lock ] && yarn --frozen-lockfile
RUN yarn build

EXPOSE 3000

CMD yarn
ENTRYPOINT yarn start
